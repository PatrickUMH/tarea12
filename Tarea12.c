#include <stdio.h>
int main(int argc, char const *argv[]) {
  int ventas=0;
  float paga=0;
  printf("Cuantos productos vendiste?   ");
  scanf("%d",&ventas);
  if (ventas>=1 && ventas<=30) {
    if (ventas<=12) {
      paga=ventas*0.05*450;
    } else if (ventas>=13 && ventas<=23) {
      paga=ventas*0.07*450;
    } else if (ventas>=24 && ventas<=30) {
      paga=ventas*0.07*435;
    }
  } else if (ventas>30) {
    paga=ventas*0.1*435;
  }
  printf("Tu paga es: %.2f\n",paga );
  return 0;
}
